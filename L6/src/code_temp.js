//code_temp.js
const min = -50, max = 150, err = -300;

const tempReader = (reading) => {
    console.log(`reading: ${reading}`)
    if (isNaN(reading)) {
        return err; // 1
    } else {
        if (min <= reading && reading <= max) {
            return reading; // 2
        } else {
            return err; // 3
        }
    }
}

module.exports = { min, max, err, tempReader };