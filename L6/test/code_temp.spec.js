// code_temp.spec.js
const { min, max, err, tempReader } = require('../src/code_temp');
const expect = require('chai').expect;

describe('Temp readings', () => {
    it('handless all edge cases', () => {
        expect(tempReader(min - 1)).to.equal(err);
        expect(tempReader(max)).to.equal(max);
        expect(tempReader("a")).to.equal(err);

        expect(tempReader(Number.MAX_VALUE +1)).to.equal(err);
    })
})